package main

import (
    "fmt"
    "io/ioutil"
    "log"

    "net/http"
    "net/url"
    
    "strings"
    "errors"

    "github.com/gin-gonic/gin"
)

var allowedDomains = []string{
        "pclo-go-demo-prod.paocloud.tech",
        "localhost",
}

func validateURL(target string) error {
        u, err := url.Parse(target)
        if err != nil {
          return err
        }
        for _, domain := range allowedDomains {
          if strings.HasSuffix(u.Hostname(), domain) {
            return nil
          }
        }
        return errors.New("Invalid domain")
}

func GetDemoUsers(c *gin.Context){
    target := c.Query("target")
    err := validateURL(target)
    if err != nil {
      c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid target URL"})
      return
    }
    //err := http.Get(target)
    //if err != nil {
      //c.JSON(http.StatusInternalServerError, gin.H{"error": "Error when making the request"})
      //return
    //}
    //defer resp.Body.Close()
    //body, err := ioutil.ReadAll(resp.Body)
    //if err != nil {
      //c.JSON(http.StatusInternalServerError, gin.H{"error": "Error when reading the response"})
      //return
    //}
    c.JSON(http.StatusOK, gin.H{"id": 1,"username":"alex","email":"alex@example.com"})
}

func GetMyWanIP(c *gin.Context){
	response, err := http.Get("https://myip.payungsakpk.xyz")
	responseData, err := ioutil.ReadAll(response.Body)
    	if err != nil {
        	log.Fatal(err)
    	}
    	fmt.Println(string(responseData))
	c.JSON(http.StatusOK, gin.H{"msg": string(responseData)})
	//return string(responseData)
}

func GetHealthCheck(c *gin.Context) {
        c.JSON(http.StatusOK, gin.H{"msg": "up !!!"})
}

func GetHome(c *gin.Context) {
        c.JSON(http.StatusOK, gin.H{"msg": "pclo-go-demo up !!!"})
}

func GetDev(c *gin.Context) {
        c.JSON(http.StatusOK, gin.H{"msg": "EXIM"})
}

func APIMiddleware() gin.HandlerFunc {
        return func(c *gin.Context) {
                c.Header("Server", "Golang-Gin-API")
                c.Header("Access-Control-Allow-Origin", "*")
                c.Header("Access-Control-Allow-Credentials", "true")
                c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
                c.Header("Access-Control-Allow-Methods", "OPTIONS, GET")

                if c.Request.Method == "OPTIONS" {
                        c.AbortWithStatus(204)
                        return
                }
                //c.Request.URL.Path = "/test2"
                c.Next()
        }
}

func setupRouter() *gin.Engine {
        r := gin.New()
        r.Use(APIMiddleware())
        r.GET("/", GetHome)
        v1 := r.Group("/api")
        {
                v1.GET("/", GetHome)
                v1.GET("/dev", GetDev)
                v1.GET("/healthz", GetHealthCheck)
		v1.GET("/myip", GetMyWanIP)
                v1.GET("/GetDemoUsers", GetDemoUsers)
        }
        r.Use(gin.LoggerWithWriter(gin.DefaultWriter, "/api/healthz"))
        return r
}

func main() {
	port := "3000"
	r := setupRouter()
	r.Run(":" + port)
}
